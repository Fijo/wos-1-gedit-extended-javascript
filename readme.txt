To install just copy ./extendedjavascript.lang to /usr/share/gtksourceview-2.0/language-specs/extendedjavascript.lang and ./oblivion.xml to /usr/share/gtksourceview-2.0/styles/oblivion.xml and restart gedit.
To use this custom language fully, you have to use the "Oblivion" theme.
